#Python with Arduino Servo motors

# import libraries
import serial  
import cv2
import face_recognition
import PySimpleGUI as sg
import time

cap = cv2.VideoCapture(0)

layout = [
    [sg.Text('Servo controller', size=(40,1), justification='center', font='Arial 20')],
    [sg.Image(filename='', key='-IMAGE-')],
    [sg.Text('Coordinates: ', size=(15,1)),
     sg.Text(size=(15,1), key='-COOR-'),
     sg.Text('Position: ', size=(15,1)),
     sg.Text(size=(15,1), key='-POS-')]
]

image_of_face = face_recognition.load_image_file('Orinta Krivaite.jpg')
face_encoding = face_recognition.face_encodings(image_of_face) [0]

#Create array of encodings and names
know_face_encodings = [
    face_encoding
    ]

know_face_names = [
    "Orinta Krivaite"
    ]

window = sg.Window('Servo controller', layout=layout)
record = False
current_time = time.time()
arduino = serial.Serial('COM8', 9600)   # create serial object named arduino
position_1 = '90'
position_2 = '90'
right = 0
name = 'Unknown Person'

def Cord_to_pos_x(x):
    pos_1 = (x * 180) / 640
    return int(pos_1)

def Cord_to_pos_y(y):
    pos_2 = ((y * 130) / 480) + 50
    return int(pos_2)
    
while True:                                             
        event, values = window.read(timeout=30)
        _, frame = cap.read()
        #h, w = frame.shape[:2] 480x640
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        face_locations = face_recognition.face_locations(frame)
        face_encodings = face_recognition.face_encodings(frame, face_locations)
        
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        
        for(top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
            matches = face_recognition.compare_faces(known_face_encodings=know_face_encodings, face_encoding_to_check = face_encoding)
    
            name = 'Unknown Person'
    
            if True in matches:
                first_match_index = matches.index(True)
                name = know_face_names[first_match_index]
    
            # Draw Box
            cv2.rectangle(frame, (left, top), (right, bottom), (0, 255, 0), 3)
            # Draw label
            cv2.putText(frame,name,(left+20,bottom+20),fontFace=cv2.FONT_HERSHEY_SIMPLEX,fontScale=0.5,color=(0,0,0))
            center_x = ((right - left) / 2) + left
            center_y = ((bottom - top) / 2) + top
            window["-COOR-"].update('{}, {}'.format(center_x,center_y))
            
        if time.time() - current_time >= 2 and face_locations:
            print('Time: {}'.format(time.time() - current_time))
            print('Center x: {} Center y: {}'.format(center_x, center_y))
            pos_1 = Cord_to_pos_x(center_x)
            pos_2 = Cord_to_pos_y(center_y)
            print('Pos 1: {} Pos 2: {}'.format(pos_1, pos_2))
            window["-POS-"].update('{}, {}'.format(pos_1,pos_2))
            str_pos1 = str(pos_1)
            str_pos2 = str(pos_2)
            arduino.write(str.encode(str_pos1))              
            reachedPos = str(arduino.readline())
            print(reachedPos) 
            arduino.write(str.encode(str_pos2))
            reachedPos = str(arduino.readline())            
            print(reachedPos) 
            current_time = time.time()
        elif time.time() - current_time >= 2:
            arduino.write(str.encode(position_1))
            reachedPos = str(arduino.readline())
            print(reachedPos)
            arduino.write(str.encode(position_2)) 
            reachedPos = str(arduino.readline())
            print(reachedPos)             
            posint = int(position_1)
            window["-POS-"].update('{}, {}'.format(position_1,position_2))
            if posint == 180:
                right = 1
            elif posint == 0:
                right = 0
            
            if right == 0:
                posint += 10
                position_1 = str(posint)
            elif right == 1:
                posint -= 10
                position_1 = str(posint)
                
        imgbytes = cv2.imencode('.png',frame)[1].tobytes()
        
        window['-IMAGE-'].update(data=imgbytes)
        
        if event == sg.WIN_CLOSED:
            break

cap.release()
cv2.destroyAllWindows() 