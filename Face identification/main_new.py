import cv2
import numpy as np
import dlib
from PIL import Image
import PySimpleGUI as sg
import face_recognition
import matplotlib.pyplot as plt


cap = cv2.VideoCapture(0)

image_of_face = face_recognition.load_image_file('./img/known/Orinta Krivaite.jpg')
face_encoding = face_recognition.face_encodings(image_of_face) [0]

#Create array of encodings and names
know_face_encodings = [
    face_encoding
]

know_face_names = [
    "Orinta Krivaite"
]

dis = []

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("C:/Users/orint/Documents/Uni7Sem/Robotizuotu_sistemu_projektas/Projektas/shape_predictor_68_face_landmarks.dat")

layout = [
    [sg.Text('Face recognition', size=(40,1), justification='center', font='Arial 20')],
    [sg.Image(filename='', key='-IMAGE-')]
]

window = sg.Window('Face regnition', layout=layout)
record = False

while True:
    event, values = window.read(timeout=30)
    _, frame = cap.read()
    
    #find faces in test image
    face_locations = face_recognition.face_locations(frame)
    face_encodings = face_recognition.face_encodings(frame, face_locations)
    
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    
    #faces = detector(gray)
    
    for(top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
        matches = face_recognition.compare_faces(known_face_encodings=know_face_encodings, face_encoding_to_check = face_encoding)
    
        name = 'Unknown Person'
    
        if True in matches:
            first_match_index = matches.index(True)
            name = know_face_names[first_match_index]
        
        dis.append(face_recognition.face_distance(know_face_encodings[first_match_index],face_encodings))
    
        # Draw Box
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 255, 0), 3)
        # Draw label
        cv2.putText(frame,name,(left,bottom),fontFace=cv2.FONT_HERSHEY_SIMPLEX,fontScale=0.5,color=(0,0,0))
        #cv2.putText(frame,'{}'.format(dis),(left+50,bottom+50),fontFace=cv2.FONT_HERSHEY_SIMPLEX,fontScale=0.5,color=(0,0,0))
    
    imgbytes = cv2.imencode('.png',frame)[1].tobytes()
    
    window['-IMAGE-'].update(data=imgbytes)
    
    if event == sg.WIN_CLOSED:
        break

plt.plot(dis)
plt.ylabel('distance')
plt.show()
    
cap.release()
cv2.destroyAllWindows() 