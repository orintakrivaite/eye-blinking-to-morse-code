import cv2
import numpy as np
import dlib
import time
from math import hypot
import PySimpleGUI as sg

cap = cv2.VideoCapture(0)

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("/shape_predictor_68_face_landmarks.dat")

def midpoint(p1, p2):
    return int((p1.x + p2.x)/2), int((p1.y + p2.y)/2) 

def GetEyeBlinkingRatio(eye_points, facial_landmarks):
    left_point = (facial_landmarks.part(eye_points[0]).x, facial_landmarks.part(eye_points[0]).y)
    right_point = (facial_landmarks.part(eye_points[3]).x, facial_landmarks.part(eye_points[3]).y)
    center_top = midpoint(facial_landmarks.part(eye_points[1]), facial_landmarks.part(eye_points[2]))
    center_bottom = midpoint(facial_landmarks.part(eye_points[5]), facial_landmarks.part(eye_points[4]))
        
    hor_line = cv2.line(frame, left_point, right_point, (0,255,0), 2)
    ver_line = cv2.line(frame, center_top, center_bottom, (0,255,0), 2)
        
    hor_line_lenght = hypot((left_point[0]-right_point[0]), (left_point[1]-right_point[1]))
    ver_line_lenght = hypot((center_top[0]-center_bottom[0]), (center_top[1]-center_bottom[1]))
        
    ratio = hor_line_lenght/ver_line_lenght
    return ratio

def MorseConvertion(txt):
    
    d = {'A':'.-','B':'-...','C':'-.-.','D':'-..','E':'.',
         'F':'..-.','G':'--.','H':'....','I':'..','J':'.---',
         'K':'-.-','L':'.-..','M':'--','N':'-.','O':'---',
         'P':'.--.','Q':'--.-','R':'.-.','S':'...','T':'-',
         'U':'..-','V':'...-','W':'.--','X':'-..-','Y':'-.--',
         'Z':'--..', ' ':'.....'}
    translation = ''

    if txt.startswith('.') or txt.startswith('-'):
        # Swap key/values in d:
        d_encrypt = dict([(v, k) for k, v in d.items()])
        # Morse code is separated by empty space chars
        translation = d_encrypt.get(txt)
    return translation

blinking_ratio = 0
flag = 0
START = 0
START_OPEN = 0       
LIMIT = 0.15
LIMIT_LONG = 0.75
LIMIT_DELETE = 2
LIMIT_DELETE_LAST = 5
LIMIT_NEXT = 3
TOTAL = 0
TOTAL_LONG = 0
TEXT = ""
txt = ""

layout = [
    [sg.Text('EYE BLINKING TO MORSE', size=(40,1), justification='center', font='Arial 20')],
    [sg.Image(filename='', key='-IMAGE-')],
    [sg.Text('TimeAtm: {:.2f}'.format(time.time() - START_OPEN), key='-TIME_OPEN-'),
     sg.Text('Time: {:.2f}'.format(time.time() - START), key='-TIME_CLOSED-')],
    [sg.Text('Morse: {}'.format(TEXT), key='-MORSE-', size=(30,1), font=("Helvetica", 25))],
    [sg.Text('ABC: {}'.format(txt), key='-ABC-', size=(30,1), font=("Helvetica", 25))],
    [sg.Text('EYE: {}'.format(blinking_ratio), key='-Ratio-', size=(30,1), font=("Helvetica", 25))]
]

window = sg.Window('Morse translator', layout=layout)
record = False

while True:
    event, values = window.read(timeout=30)
    _, frame = cap.read()
    imgbytes = cv2.imencode('.png',frame)[1].tobytes()
    window['-IMAGE-'].update(data=imgbytes)
    window['-TIME_OPEN-'].update(value='TimeAtm: {:.2f}'.format(time.time() - START_OPEN))
    window['-TIME_CLOSED-'].update(value='Time: {:.2f}'.format(time.time() - START))
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    
    faces = detector(gray)
    for face in faces:
        #x, y = face.left(), face.top()
        #x1, y1 = face.right(), face.bottom()
        #cv2.rectangle(frame, (x,y), (x1,y1), (0,255,0), 2)
        landmarks = predictor(gray, face)
        
        left_eye_ratio = GetEyeBlinkingRatio([36,37,38,39,40,41], landmarks)
        right_eye_ratio = GetEyeBlinkingRatio([42,43,44,45,46,47], landmarks)
        blinking_ratio = (left_eye_ratio+right_eye_ratio)/2
        window['-Ratio-'].update('EYE: {}'.format(blinking_ratio))
        #print(blinking_ratio)
        
        if blinking_ratio > 4.5 and flag == 0:
            START = time.time()
            START_OPEN = 0
            flag = 1 #akys buvo uzmerktos
        if flag == 1 and blinking_ratio < 4.5:
            START_OPEN = time.time()
            if START != 0 and time.time() - START > LIMIT and time.time() - START < LIMIT_LONG:
                TOTAL += 1
                TEXT += "."
                window['-MORSE-'].update(value='Morse: {}'.format(TEXT))
              
            elif START != 0 and time.time() - START > LIMIT_LONG and time.time() - START < LIMIT_DELETE:
                TOTAL_LONG += 1
                TEXT += "-"
                window['-MORSE-'].update(value='Morse: {}'.format(TEXT))
            elif START != 0 and time.time() - START > LIMIT_DELETE and time.time() - START < LIMIT_DELETE_LAST:
                TEXT = ""
                window['-MORSE-'].update(value='Morse: {}'.format(TEXT))
            elif START != 0 and time.time() - START > LIMIT_DELETE_LAST:
                txt = txt[:-1]
                window['-ABC-'].update(value='ABC: {}'.format(txt))
            
            START = 0
            flag = 0 #akys buvo atmerktos
        if flag == 0 and blinking_ratio < 4.5:
            if START_OPEN != 0 and time.time() - START_OPEN > LIMIT_NEXT:
                txt += MorseConvertion(TEXT)
                TEXT = "" 
                window['-ABC-'].update(value='ABC: {}'.format(txt))
                window['-MORSE-'].update(value='Morse: {}'.format(TEXT))
    
    if event == sg.WIN_CLOSED:
        break
    
    if cv2.waitKey(30) == ord('q'):
        break
    
cap.release()
cv2.destroyAllWindows() 
