# Eye blinking to morse code

## Description
Using a camera you can convert eye blinking to morse code and then to text.


Short blink 0.15 s - dot(.)

Long blink 0.75s - dash(-)


Blinking is detected based on Face landmarks (atached file). 

blinking_ratio_threshold - editable.

Resourse: http://vision.fe.uni-lj.si/cvww2016/proceedings/papers/05.pdf

## Usage
Using Python 3.8.
To use the shape_predictor_68_face_landmarks.dat file change line 11 with the correct path.
