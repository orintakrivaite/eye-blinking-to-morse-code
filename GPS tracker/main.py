import re
from PySimpleGUI.PySimpleGUI import Window
import serial
import math
import io
import os
import PySimpleGUI as sg
import cv2

ser = serial.Serial("COM9", 9600)
new_data = []
data = []
data1 = []
data2 = []
flag = 0

N1 = 54.9066
N2 = 54.9031
E1 = 23.9515
E2 = 23.9636

map = cv2.imread('image.png')
h, w = map.shape[:2]

def ddmm_mmmmToDeg (Data):
    deg = "NaN"
    if Data != '':
        dd = math.floor(float(Data)/100)
        mm = float(Data)-dd*100
        deg = dd+mm/60
    return deg

def coordinatesX(w, x, E1, E2):
    xcor = 0
    if x != 'NaN':
        xcor = int(((w*(x-E1))/(E2-E1)))
    return xcor

def coordinatesY(h, y, N1, N2):
    ycor = 0
    if y != 'NaN':
        ycor = int(((h*(y-N1))/(N2-N1)))
    return ycor

def Time(Data):
    Out = ""
    if Data != '':
	    hh = math.floor(float(Data)/10000)
	    mm = math.floor((float(Data)-hh*10000)/100)
	    ss = float(Data)-hh*10000-mm*100
	    Time = hh*60*60+mm*60+ss
	    Out = [int(Time),int(hh),int(mm),int(ss)]
    return Out

class InterfaceGrafica:
    def __init__(self):
        layout = [ 
            [sg.Text('GPS Tracker', size=(40,1), justification='center', font='Arial 20')],
            [sg.Image(filename='', key='-IMAGE-')],
            [sg.Text('Latitude: ', size=(15,1), enable_events=True),
            sg.Text(size=(15,1), key='-LAT-'),
            sg.Text('Longditude: ',size=(15,1)),
            sg.Text(size=(15,1), key='-LONG-')],
            [sg.Text('UTC time: ', size=(15,1)),
            sg.Text(size=(15,1), key='-TIME-'),
            sg.Text('Speed: ', size=(15,1)),
            sg.Text(size=(15,1), key='-SPEED-')],
            [sg.Text('Status: ', size=(15,1)),
            sg.Text(size=(15,1), key='-STATUS-'),
            sg.Text('Satalite: ', size=(15,1)),
            sg.Text(size=(15,1), key='-SATALITE-')],
            [sg.Button('Start', size=(10,1), font='Arial 14'),
            sg.Button('Finish', size=(10,1), font='Arial 14'),
            sg.Button('Save', size=(10,1), font='Arial 14'),
            sg.Button('Upload', size=(10,1), font='Arial 14'),
            sg.Button('Clear', size=(10,1), font='Arial 14'),
            sg.Button('Exit', size=(10,1), font='Arial 14')]
        ]

        self.window = sg.Window( 'GPS', layout=layout, size=(800, 600) )

        while True:
            global flag
            global map
            global data
            self.event, self.values = self.window.read( timeout=200 )
            if self.event == sg.WIN_CLOSED or self.event == 'Exit':
                break
            
            if self.event == 'Start':
                global flag
                flag = 1
            
            if self.event == 'Finish':
                flag = 0
            
            if self.event == 'Clear':
                map = cv2.imread('new_uni.png')
                
            if self.event == 'Save':
                with open('data.txt', 'w') as f:
                    for i in range(0, len(data)):
                        f.write(data[i])
                        f.write('\n')
            
            if self.event == 'Upload':
                map = cv2.imread('new_uni.png')
                with open("uni.txt") as file:
                    for line in file:
                        u_data = line.rstrip()
                        upload_data = re.split(',', u_data)
                        # print(upload_data)
                        # print(upload_data[0])
                        # print(upload_data[1])
                        x = coordinatesX(w, float(upload_data[1]), E1, E2)
                        y = coordinatesY(h, float(upload_data[0]), N1, N2)
                        cv2.circle(map,(x,y),2,(0,0,255),2)
            
            leitura = ser.readline()  ##  read lines from serial
            leiturad = leitura.decode('ascii')  ##  decode those lines
            new_data = re.split(',', leiturad)  ##  split CSV into list
            if (new_data[0]=='$GPGGA'):
                print("latitude: {} longitude: {} ".format(ddmm_mmmmToDeg(new_data[2]),ddmm_mmmmToDeg(new_data[4])))
                i = len(data1)
                print(flag)
                latitude = ddmm_mmmmToDeg(new_data[2])
                longitude = ddmm_mmmmToDeg(new_data[4])
                time = Time(new_data[1])
                self.window["-LAT-"].update('{} {}'.format(round(latitude,6),new_data[3]))
                self.window['-LONG-'].update('{} {}'.format(round(longitude,6), new_data[5]))
                self.window['-TIME-'].update('{:02d}:{:02d}:{:02d}'.format(time[1],time[2],time[3]))
                self.window['-SATALITE-'].update('{}'.format(new_data[7]))
                if time[0] % 2 == 0:
                    x = coordinatesX(w, float(longitude), E1, E2)
                    y = coordinatesY(h, float(latitude), N1, N2)
                    if (flag == 1):
                        cv2.circle(map,(x,y),2,(0,0,255),2)
                        data.append('{},{}'.format(latitude,longitude))

            if (new_data[0]=='$GPRMC'):
                self.window['-SPEED-'].update('{} knots'.format(new_data[7]))
            
            if (new_data[0]=='$GPGSA'):
                if (new_data[2] == '1'):
                    Status = 'No Fix'
                elif (new_data[2] == '2'):
                    Status = '2D Fix'
                elif (new_data[2] == '3'):
                    Status = '3D Fix'
                else:
                    Status = '???'
                self.window['-STATUS-'].update('{}'.format(Status))
            imgbytes = cv2.imencode(".png", map)[1].tobytes()
            self.window["-IMAGE-"].update(data=imgbytes)
            self.window.Refresh()


InterfaceGrafica()